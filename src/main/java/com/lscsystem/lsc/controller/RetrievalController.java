package com.lscsystem.lsc.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.lscsystem.lsc.model.LifelogImage;
import com.lscsystem.lsc.model.LifelogQuery;
import com.lscsystem.lsc.model.PagingResponse;
import com.lscsystem.lsc.service.RetrievalService;
import com.lscsystem.lsc.service.ThesaurusService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.*;
import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.ExecutionException;

@RestController
@RequestMapping("/api/lsc")
public class RetrievalController {

    private final RetrievalService retrievalService;

    private final ThesaurusService thesaurusService;

    @Autowired
    public RetrievalController(RetrievalService retrievalService, ThesaurusService thesaurusService) {
        this.retrievalService = retrievalService;
        this.thesaurusService = thesaurusService;
    }

    @PostMapping("/search")
    public PagingResponse search(@RequestBody LifelogQuery query,
                                 @RequestParam(value = "page", defaultValue = "1") Integer page,
                                 @RequestParam(value = "size", defaultValue = "24") Integer size) throws ExecutionException {
        return retrievalService.performQueryPaging(query, page, size);
    }

    @GetMapping("/data/{image_id}")
    public List<LifelogImage> getAnswerByTopic(@PathVariable("image_id") String imageId,
                                               @RequestParam(value = "left", defaultValue = "0") Integer left,
                                               @RequestParam(value = "right", defaultValue = "0") Integer right) {
        return retrievalService.findRangeWithPilot(imageId, left, right);
    }

    @GetMapping("/answer/{topic}")
    public List<LifelogImage> getAnswerByTopic(@PathVariable("topic") String topic) throws IOException {
        return retrievalService.loadAnswer(topic);
    }

    @PostMapping("/answer/{topic}/{image_id}")
    public String saveAnswer(@PathVariable("topic") String topic,
                             @PathVariable("image_id") String imageId) throws IOException {
        retrievalService.saveAnswer(topic, imageId);
        return "OK";
    }

    @GetMapping("/concept")
    public Map<String, Object> getConcept() {
        Map<String, Object> res = new HashMap<>();
        res.put("concept", retrievalService.getConceptSet());
        res.put("attribute", Collections.emptyList());
        res.put("category", retrievalService.getCategorySet());
        res.put("activity", Collections.emptyList());
        res.put("location", retrievalService.getLocationSet());
        return res;
    }

    @GetMapping("/thesaurus")
    public Map<String, List<String>> getThesaurus(@RequestParam("words") List<String> words) throws BrokenBarrierException, InterruptedException, JsonProcessingException {
        return thesaurusService.findThesaurus(words);
    }

    @GetMapping("/color")
    public Map<String, List<String>> getColorOfConcepts(@RequestParam("concepts") List<String> concepts) {
        return Collections.emptyMap();
    }

    @PutMapping("/answer/{topic}")
    public String saveAnswer(@PathVariable("topic") String topic,
                             @RequestBody List<String> imageIds) throws IOException {
        retrievalService.addAnswer(topic, imageIds);
        return "OK";
    }

    @DeleteMapping("/answer/{topic}/{image_id}")
    public String deleteAnswer(@PathVariable("topic") String topic,
                               @PathVariable("image_id") String imageId) throws IOException {
        retrievalService.deleteAnswer(topic, imageId);
        return "OK";
    }

    @GetMapping("/answer/topic")
    public List<String> getTopic() throws IOException {
        return retrievalService.getAnswerTopics();
    }
}
