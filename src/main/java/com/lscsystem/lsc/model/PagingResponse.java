package com.lscsystem.lsc.model;

import lombok.Data;

import java.util.List;

@Data
public class PagingResponse {
    private List<LifelogImage> images;
    private int totalCount;

    private PagingResponse() {
    }

    public static PagingResponse of(List<LifelogImage> images, int totalCount) {
        PagingResponse response = new PagingResponse();
        response.setImages(images);
        response.setTotalCount(totalCount);
        return response;
    }
}
