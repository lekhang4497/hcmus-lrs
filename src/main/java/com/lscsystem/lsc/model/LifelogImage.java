package com.lscsystem.lsc.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.time.LocalDateTime;
import java.util.List;

@Data
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@JsonIgnoreProperties(ignoreUnknown = true)
public class LifelogImage {
    private String user;
    private List<String> attributes;
    private List<String> concepts;
    private List<String> categories;
    @JsonProperty("imgId")
    @EqualsAndHashCode.Include
    private String imageId;
    @JsonProperty("imgPath")
    private String imagePath;
    private String imageName;
    private LocalDateTime utcTime;
    private LocalDateTime localTime;
    private String timeZone;
    private String locationName;
    private boolean fromCam;
}
