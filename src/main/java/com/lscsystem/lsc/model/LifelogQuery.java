package com.lscsystem.lsc.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.List;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class LifelogQuery {
    private List<String> attributes;
    private List<String> concepts;
    private List<String> categories;
    @JsonProperty("category_accuracy")
    private int categoryAccuracy;
    @JsonProperty("from_time")
    private String fromTime;
    @JsonProperty("to_time")
    private String toTime;
    @JsonProperty("time_zone")
    private List<String> timeZone;
    @JsonProperty("location_name")
    private List<String> locationName;
}
