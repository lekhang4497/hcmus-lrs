package com.lscsystem.lsc.model;

import lombok.Data;

import java.time.LocalDateTime;

@Data
public class MinuteInfo {
    private String minuteId;
    private LocalDateTime utcTime;
    private LocalDateTime localTime;
    private String timeZone;
    private String locationName;
    private String activity;
    private int steps;
    private double calories;
    private int heartRate;
}
