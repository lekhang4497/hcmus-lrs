package com.lscsystem.lsc.service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import com.lscsystem.lsc.model.LifelogImage;
import com.lscsystem.lsc.model.LifelogQuery;
import com.lscsystem.lsc.model.MinuteInfo;
import com.lscsystem.lsc.model.PagingResponse;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.concurrent.ExecutionException;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
public class RetrievalService {

    @Value("${lifelog.answer.file}")
    private String answerFile;

    @Value("${lifelog.metadata.path}")
    private String metadataPath;

    private Map<String, LifelogImage> imageIdMap = new HashMap<>();
    private Map<String, LifelogImage> imageNameMap = new HashMap<>();
    private Map<String, MinuteInfo> minuteMap = new HashMap<>();
    private List<LifelogImage> lifelogData = new ArrayList<>();
    private Map<String, Integer> lifelogInvertedIndex = new HashMap<>();
    private final int CATEGORY_LIMIT;
    private final int ATTRIBUTE_LIMIT;
    private final int CONCEPT_LIMIT;
    private final ObjectMapper objectMapper;
    private Cache<String, List<LifelogImage>> resultCache;

    private Set<String> conceptSet = new HashSet<>();
    private Set<String> categorySet = new HashSet<>();
    private Set<String> attributeSet = new HashSet<>();
    private Set<String> locationSet = new HashSet<>();

    @Value("${lifelog.habit.food}")
    private List<String> habitFood;
    @Value("${lifelog.habit.cloth}")
    private List<String> habitCloth;
    @Value("${lifelog.habit.music}")
    private List<String> habitMusic;
    @Value("${lifelog.habit.device}")
    private List<String> habitDevice;
    @Value("${lifelog.habit.object}")
    private List<String> habitObject;

    public RetrievalService() {
        CATEGORY_LIMIT = 5;
        ATTRIBUTE_LIMIT = 10;
        CONCEPT_LIMIT = 25;
        objectMapper = new ObjectMapper();
        resultCache = CacheBuilder.newBuilder()
                .maximumSize(3)
                .build();
    }

    @PostConstruct
    public void init() {
//        try {
//            generateAnswerCsv();
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
        initSystem();
        // Load habit-based concepts
        String[] detectorFiles = {"device.txt", "cloth.txt", "food.txt", "music.txt", "sport.txt"};
        for (String file : detectorFiles) {
            String fileName = metadataPath + "obj_detector/" + file;
            loadHabitBasedConcept(fileName);
            System.out.println("Loaded habit-base concepts from: " + file);
        }
    }

    public Set<String> getConceptSet() {
        Set<String> result = new HashSet<>(conceptSet);
        result.addAll(habitFood.stream().map(String::toLowerCase).collect(Collectors.toList()));
        result.addAll(habitCloth.stream().map(String::toLowerCase).collect(Collectors.toList()));
        result.addAll(habitMusic.stream().map(String::toLowerCase).collect(Collectors.toList()));
        result.addAll(habitDevice.stream().map(String::toLowerCase).collect(Collectors.toList()));
        result.addAll(habitObject.stream().map(String::toLowerCase).collect(Collectors.toList()));
        return result;
    }

    public Set<String> getCategorySet() {
        return categorySet;
    }

    public Set<String> getLocationSet() {
        return locationSet;
    }

    public Set<String> getAttributeSet() {
        return attributeSet;
    }

    private List<MinuteInfo> loadMinuteData(String fileName) {
        try (
                Reader reader = Files.newBufferedReader(Paths.get(fileName));
                CSVParser csvParser = new CSVParser(reader, CSVFormat.DEFAULT
                        .withFirstRecordAsHeader()
                        .withIgnoreHeaderCase()
                        .withTrim())
        ) {
            List<MinuteInfo> result = new ArrayList<>();
            for (CSVRecord record : csvParser) {
                MinuteInfo minuteInfo = new MinuteInfo();
                minuteInfo.setActivity(record.get("activity"));

                String stepsStr = record.get("steps");
                minuteInfo.setSteps(stepsStr.equals("NULL") ? -1 : Integer.parseInt(stepsStr));

                String caloriesStr = record.get("calories");
                minuteInfo.setCalories(caloriesStr.equals("NULL") ? -1 : Double.parseDouble(caloriesStr));

                String heartRateStr = record.get("heart_rate");
                minuteInfo.setHeartRate(heartRateStr.equals("NULL") ? -1 : Integer.parseInt(heartRateStr));

                minuteInfo.setMinuteId(record.get("minute_ID"));
                minuteInfo.setTimeZone(record.get("time_zone"));
                minuteInfo.setLocationName(record.get("name"));
                locationSet.add(record.get("name"));

                String utcTimeStr = record.get("utc_time").replace("_UTC", "");
                String localTimeStr = record.get("local_time");
                DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyyMMdd_HHmm");
                minuteInfo.setUtcTime(LocalDateTime.parse(utcTimeStr, formatter));
                minuteInfo.setLocalTime(LocalDateTime.parse(localTimeStr, formatter));

                result.add(minuteInfo);
            }
            return result;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    private void initSystem() {
        System.out.println("Start loading core metadata");
        String csv1 = metadataPath + "u1_concepts.csv";
        String csv2 = metadataPath + "u2_concepts.csv";
        String minuteCsv1 = metadataPath + "u1.csv";
        String minuteCsv2 = metadataPath + "u2.csv";

        List<MinuteInfo> minuteList = loadMinuteData(minuteCsv1);
        assert minuteList != null;
//        minuteList.addAll(Objects.requireNonNull(loadMinuteData(minuteCsv2)));
        this.minuteMap = minuteList.stream().collect(Collectors.toMap(MinuteInfo::getMinuteId, o -> o, (oldValue, newValue) -> newValue));

        List<LifelogImage> imageList = readConceptCsvFile(csv1, "u1");
        assert imageList != null;
//        imageList.addAll(Objects.requireNonNull(readConceptCsvFile(csv2,"u2")));

        this.imageIdMap = imageList.stream().collect(Collectors.toMap(LifelogImage::getImageId, o -> o, (oldValue, newValue) -> newValue));
        this.imageNameMap = imageList.stream().collect(Collectors.toMap(LifelogImage::getImageName, o -> o, (oldValue, newValue) -> newValue));
        imageList.sort(Comparator.comparing(LifelogImage::getUtcTime));
        this.lifelogData = imageList;

        // Build inverted index
        for (int i = 0; i < this.lifelogData.size(); i++) {
            this.lifelogInvertedIndex.put(this.lifelogData.get(i).getImageId(), i);
        }
        System.out.println("Finish loading core metadata");
    }

    private void loadHabitBasedConcept(String fileName) {
        double threshold = 0.8;
        try (Stream<String> stream = Files.lines(Paths.get(fileName))) {
            stream.forEach(line -> {
                String[] lineSplit = line.split(" ");
                String imageName = lineSplit[0];
                String conceptName = lineSplit[1].trim().toLowerCase();
                double confidence = Double.parseDouble(lineSplit[2]);
                if (confidence > threshold) {
                    LifelogImage image = imageNameMap.get(imageName);
                    if (image == null) {
                        System.out.println("Not found image: " + imageName);
                    } else {
                        image.getConcepts().add(conceptName);
                    }
                }
            });
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public List<LifelogImage> findRangeWithPilot(String pilotId, int left, int right) {
        Integer pilot = this.lifelogInvertedIndex.get(pilotId);
        if (pilot == null)
            return Collections.emptyList();
        int from = pilot - left;
        from = from < 0 ? 0 : from;
        int to = pilot + right;
        to = to > this.lifelogData.size() - 1 ? this.lifelogData.size() - 1 : to;
        return this.lifelogData.subList(from, to + 1);
    }

    private List<String> getMetaData(CSVRecord record, String name, int limit) {
        List<String> result = new ArrayList<>();
        for (int i = 1; i <= limit; i++) {
            String value = record.get(String.format("%s%02d", name, i));
            if (value.equals("NULL"))
                break;
            result.add(value);
        }
        return result;
    }

    private boolean checkIntersect(Collection<String> a, Collection<String> b) {
        return a.stream().anyMatch(b::contains);
    }

//    private void loadAnswerWithId() throws IOException {
//        Map<String, List<String>> answerId = objectMapper.readValue(new File(answerFile), new TypeReference<Map<String, List<String>>>() {
//        });
//        Map<String, List<LifelogImage>> answer = new HashMap<>();
//        for (Map.Entry<String, List<String>> entry : answerId.entrySet()) {
//            List<LifelogImage> images = entry.getValue().stream().map(s -> imageIdMap.get(s)).collect(Collectors.toList());
//            answer.put(entry.getKey(), images);
//        }
//        objectMapper.writeValue(new File("answer.json"), answer);
//    }

    public void saveAnswer(String topic, String imageId) throws IOException {
        Map<String, List<String>> answer = loadAnswerId();
        answer.get(topic).add(imageId);
        objectMapper.writeValue(new File(answerFile), answer);
    }

    public void deleteAnswer(String topic, String imageId) throws IOException {
        Map<String, List<String>> answer = loadAnswerId();
        answer.get(topic).remove(imageId);
        objectMapper.writeValue(new File(answerFile), answer);
    }

    private Map<String, List<String>> loadAnswerId() throws IOException {
        return objectMapper.readValue(new File(answerFile), new TypeReference<Map<String, List<String>>>() {
        });
    }

    public List<String> getAnswerTopics() throws IOException {
        return loadAnswerId().keySet().stream().sorted().collect(Collectors.toList());
    }

    public Map<String, List<LifelogImage>> loadAnswer() throws IOException {
        Map<String, List<String>> answerId = loadAnswerId();
        Map<String, List<LifelogImage>> answer = new HashMap<>();
        for (Map.Entry<String, List<String>> entry : answerId.entrySet()) {
            List<LifelogImage> images = entry.getValue().stream().map(s -> imageIdMap.get(s)).collect(Collectors.toList());
            answer.put(entry.getKey(), images);
        }
        return answer;
    }

    public List<LifelogImage> loadAnswer(String topic) throws IOException {
        return loadAnswerId().getOrDefault(topic, Collections.emptyList()).stream().map(s -> imageIdMap.get(s)).collect(Collectors.toList());
    }

    public List<LifelogImage> performQuery(LifelogQuery query) throws ExecutionException {
        return resultCache.get(query.toString(), () -> searchSimple(query));
    }

    public PagingResponse performQueryPaging(LifelogQuery query, int page, int size) throws ExecutionException {
        int offset = (page - 1) * size;
        int to = offset + size;
        List<LifelogImage> images = performQuery(query);
        int totalCount = images.size();
        if (offset > images.size() - 1)
            return PagingResponse.of(Collections.emptyList(), totalCount);
        else
            return PagingResponse.of(images.subList(offset, to > images.size() ? images.size() : to), totalCount);
    }

    public List<LifelogImage> searchSimple(LifelogQuery query) {
        List<LifelogImage> result = new ArrayList<>();
        LocalTime queryFromTime = query.getFromTime() != null ? LocalTime.parse(query.getFromTime(), DateTimeFormatter.ofPattern("HHmmss")) : null;
        LocalTime queryToTime = query.getToTime() != null ? LocalTime.parse(query.getToTime(), DateTimeFormatter.ofPattern("HHmmss")) : null;
        for (LifelogImage image : lifelogData) {
            if (query.getAttributes() != null && !checkIntersect(query.getAttributes(), image.getAttributes())) {
                continue;
            }
            if (query.getCategories() != null && !checkIntersect(query.getCategories(), image.getCategories().subList(0, 5))) {
                continue;
            }
            if (query.getConcepts() != null && !image.getConcepts().containsAll(query.getConcepts())) {
                continue;
            }
            if (queryFromTime != null && image.getLocalTime().toLocalTime().isBefore(queryFromTime)) {
                continue;
            }
            if (queryToTime != null && image.getLocalTime().toLocalTime().isAfter(queryToTime)) {
                continue;
            }
            if (query.getTimeZone() != null && !query.getTimeZone().contains(image.getTimeZone())) {
                continue;
            }
            if (query.getLocationName() != null && !query.getLocationName().contains(image.getLocationName())) {
                continue;
            }
            result.add(image);
        }
        return result;
    }

    private void generateAnswerCsv() throws IOException {
        String fileName = "BASELINE_NEW_RUN_2.csv";
        FileWriter fileWriter = new FileWriter(fileName);
        PrintWriter printWriter = new PrintWriter(fileWriter);
        Map<String, List<String>> answerId = loadAnswerId();
        for (int i = 1; i <= 10; i++) {
            String topic = String.format("%03d", i);
            List<String> validateList = answerId.get(topic).stream().distinct().collect(Collectors.toList());
            List<String> ids = validateList.subList(0, validateList.size() > 10 ? 10 : validateList.size());
            for (String id : ids) {
                printWriter.println(String.format("%s,%s,%s", i, id, "1.00"));
            }
        }
        printWriter.close();

    }

    private String replaceLast(String text, String regex, String replacement) {
        return text.replaceFirst("(?s)" + regex + "(?!.*?" + regex + ")", replacement);
    }

    private List<LifelogImage> readConceptCsvFile(String fileName, String user) {
        int sampleMinutIdLength = "u1_20180503_0617".length();
        try (
                Reader reader = Files.newBufferedReader(Paths.get(fileName));
                CSVParser csvParser = new CSVParser(reader, CSVFormat.DEFAULT
                        .withFirstRecordAsHeader()
                        .withIgnoreHeaderCase()
                        .withTrim())
        ) {
            List<LifelogImage> result = new ArrayList<>();
            for (CSVRecord record : csvParser) {
                LifelogImage image = new LifelogImage();
                List<String> attributeList = getMetaData(record, "attribute_top", ATTRIBUTE_LIMIT);
                List<String> categoryList = getMetaData(record, "category_top", CATEGORY_LIMIT);
                List<String> conceptList = getMetaData(record, "concept_class_top", CONCEPT_LIMIT);

                categoryList = categoryList.stream().map(s -> {
                    int idx = s.indexOf("/");
                    if (idx != -1)
                        return s.substring(0,idx);
                    else
                        return s;
                }).collect(Collectors.toList());

                this.conceptSet.addAll(conceptList);
                this.categorySet.addAll(categoryList);
                this.attributeSet.addAll(attributeList);

                image.setImageId(record.get("image_id"));
                image.setAttributes(attributeList);
                image.setCategories(categoryList);
                image.setConcepts(conceptList);
                image.setUser(user);
                if (image.getImageId().contains("cam"))
                    image.setFromCam(true);
                else
                    image.setFromCam(false);
                // Set image path
                String rawImgPath = record.get("image_path").replace(" ", "_");
                String imgPath;
                if (image.isFromCam()) {
                    imgPath = "http://178.128.231.164:8080/resources/" + user + "/" + replaceLast(rawImgPath, "_", "%20");
                } else {
                    imgPath = "http://178.128.231.164:8080/resources/" + user + "/Autographer/" + rawImgPath;
                }
                image.setImagePath(imgPath);
                // Get date time from minute map
                String minuteId = image.getImageId().substring(0, sampleMinutIdLength);
                MinuteInfo minuteInfo = this.minuteMap.get(minuteId);
                assert minuteInfo != null;
                image.setUtcTime(minuteInfo.getUtcTime());
                image.setLocalTime(minuteInfo.getLocalTime());
                image.setTimeZone(minuteInfo.getTimeZone());
                image.setLocationName(minuteInfo.getLocationName());
                // Parse image name
                String[] pathSplit = rawImgPath.split("/");
                String lastElement = pathSplit[pathSplit.length - 1].replace(" ", "_");
                image.setImageName(lastElement.substring(0, lastElement.lastIndexOf(".")));


                result.add(image);
            }
            return result;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public void addAnswer(String topic, List<String> imageIds) throws IOException {
        Map<String, List<String>> answer = loadAnswerId();
        List<String> newList = answer.getOrDefault(topic, new ArrayList<>());
        newList.addAll(imageIds);
        newList = newList.stream().distinct().collect(Collectors.toList());
        answer.put(topic, newList);
        objectMapper.writeValue(new File(answerFile), answer);
    }
}
