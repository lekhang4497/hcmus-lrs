package com.lscsystem.lsc.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.lscsystem.lsc.model.WordInfo;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriBuilder;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;
import java.util.stream.Collectors;

@Service
public class ThesaurusService {

    public Map<String, List<String>> findThesaurus(List<String> words) throws BrokenBarrierException, InterruptedException, JsonProcessingException {
        RestTemplate restTemplate = new RestTemplate();
        int numThreads = words.size();
        Map<String, List<String>> result = new HashMap<>();
        CyclicBarrier cyclicBarrier = new CyclicBarrier(numThreads + 1);

        for (String word : words) {
            Thread worker = new Thread(() -> {
                UriComponents uri = UriComponentsBuilder
                        .fromHttpUrl("https://api.datamuse.com/words")
                        .queryParam("ml", word)
                        .queryParam("max", 10)
                        .build();
                WordInfo[] thesaurus = restTemplate.getForObject(uri.toUriString(), WordInfo[].class);
                if (thesaurus != null) {
                    result.put(word, Arrays.stream(thesaurus).map(wordInfo -> wordInfo.getWord().replace("-", "_")).collect(Collectors.toList()));
                }
                try {
                    cyclicBarrier.await();
                } catch (InterruptedException | BrokenBarrierException e) {
                    e.printStackTrace();
                }
            });
            worker.start();
        }
        cyclicBarrier.await();
        System.out.println(new ObjectMapper().writeValueAsString(result));
        return result;
    }
}
