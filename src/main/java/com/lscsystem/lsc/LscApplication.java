package com.lscsystem.lsc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LscApplication {

	public static void main(String[] args) {
		SpringApplication.run(LscApplication.class, args);
	}

}
